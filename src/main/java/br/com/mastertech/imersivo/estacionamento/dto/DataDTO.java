package br.com.mastertech.imersivo.estacionamento.dto;

import java.util.Date;

public class DataDTO {
	
	private Date data;

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
}
